@extends('layouts.app')

@section('content')
    <div>
        <h2 class="text-center mt-3" style="color: red;">Featured Posts</h2>

        @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="card text-center mb-3">
                    <div class="card-body">
                        <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
         
                        <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
    
                    </div>
                </div>
            @endforeach
        @else
            <div>
                <h3>Opps! There are no posts to show</h3>
                
            </div>
        @endif
    </div>
@endsection
