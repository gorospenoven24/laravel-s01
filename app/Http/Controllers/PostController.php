<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// to check if the user is login
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    //retrieve 3 randomize posts
    public function welcome() {
        // get all posts from the database
        $posts = Post::inRandomOrder()
            ->limit(3)
            ->get();
            // $posts = Post::inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    public function create() {
        return view('posts.create');
    }

    public function store(Request $request) {
        // if the re is an authenticated user
       if(Auth:: user()) {
        //  create a new Post object
            $post = new Post;

            // defibe the properties of the $post object using received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');

            // get the id of the auth user set it as the valu of the user_id
            $post->user_id = (Auth::user()->id);
            // save the post obejct to the database
            $post->save();

            return redirect('/posts');

       } else {
           return redirect('login');

       }
    }

    public function index() {
        // get all posts from the database
        $posts = Post::all();
        $posts = Post::where('isActive',1)->get();
        return view('posts.index')->with('posts', $posts);
    }

    public function myPosts() {
        // if the user is login
        if(Auth::user()){
            $posts = Auth::user()->posts;// retrieve the users own post

            return view('posts.index') ->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    public function show($id) {
       
       $post = Post::find($id);
       return view('posts.show')->with('post',$post);
    }

    public function edit($id) { 
       $post = Post::find($id);
       return view('posts.edit')->with('post',$post);
    }

    public function update(Request $request, $id) { 
        $post = Post::find($id);
        
        if(Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        return redirect('/posts');
     }

     public function archive($id) { 
        $post = Post::find($id);
       
        if(Auth::user()->id == $post->user_id) {
            $post->isActive = 0;
            $post->save();
        }
        return redirect('/posts');
     }

     public function like($id) {
         $post = Post::find($id);
         $user_id = Auth::user()->id;

         //check if the post not is not equal to the login user
         if($post->user_id != $user_id) {
            //  check if the user already like the post
            if($post->likes->contains("user_id", $user_id)) {
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else {
                //if the user has not liked the post yet
                $postLike = new PostLike;

                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                $postLike->save();
            }

            return redirect("/posts/$id");
         }
     }

     public function comment(Request $request,$id) {
        $post = Post::find($id);
        $user_id = Auth::user()->id;
        
        if(Auth:: user()) {
            $postComment = new PostComment;
            $postComment->post_id = $post->id;
            $postComment->user_id = $user_id;
            $postComment->content = $request->input('content');
            $postComment->user_id = (Auth::user()->id);
            $postComment->save();

            return redirect("/posts/$id");
    
        }
    }
 
}
