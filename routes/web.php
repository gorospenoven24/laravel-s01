<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/', [PostController::class, 'welcome']);

// route for crreate new post
Route::get('posts/create', [PostController::class, 'create']);

// route for route whrein form data can be sent via POST method
Route::post('/posts', [PostController::class, 'store']);

// route that will retirn a vuew containing all post
Route::get('/posts', [PostController::class, 'index']);

// return yur own post by auth user
Route::get('/posts/myPosts', [PostController::class, 'myPosts']);


// route that will show specific posts view
Route::get('/posts/{id}', [PostController::class, 'show']);

Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// route that will update the post
Route::put('/posts/{id}', [PostController::class, 'update']);

// route that will delete post
Route::delete('/posts/{id}', [PostController::class, 'archive']);


// route that will enable users to like/unlike posts
Route::put('/posts/{id}/like', [PostController::class, 'like']);

// route that will create comment posts
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);

Auth::routes();

Route::get('/home', [HomeController::class, 'index']);
